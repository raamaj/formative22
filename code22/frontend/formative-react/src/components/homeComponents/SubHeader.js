import React, { Component } from "react";

export default class SubHeader extends Component {
    render() {
        return(
            <div id={"subheader"}>
                <div className={"row"}>
                    <div className={"twelve columns"}>
                        <p className={"text-center"}>
                            <a href="/">
                                "Vision is the art of seeing what is invisible to others" - Jonathan Swift
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}