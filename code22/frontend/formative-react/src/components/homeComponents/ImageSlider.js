import React, { Component } from "react";
import image from "../../images/webImage/2.jpg"

export default class ImageSlider extends Component {
    render() {
        return(
            <>
                <div id="ei-slider" className={"ei-slider"}>
                    <ul className={"ei-slider-large"}>
                        <li>
                        <img src={image} alt="image01" className={"responsiveslide"}/>
                        <div className={"ei-title"}>
                            <h2>Dare to</h2>
                            <h3>Dream</h3>
                        </div>
                        </li>
                    </ul>
                </div>
            </>
        )
    }
}