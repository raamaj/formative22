import React, { Component } from "react";

export default class Quotes extends Component {
    render() {
        return(
            <div className="row">
                <div className="twelve columns">
                    <div id="testimonials">
                        <blockquote>
                            <p>
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis." <cite>Martin - NY</cite>
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>
        )
    }
}