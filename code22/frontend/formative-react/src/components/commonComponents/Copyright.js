import React, { Component } from "react";

export default class Copyright extends Component {
    render() {
        return(
            <div className="copyright">
                <div className="row">
                    <div className="six columns">
                        &copy;<span className="small"> Copyright Your Agency Name</span>
                    </div>
                    <div className="six columns">
                        <span className="small floatright">Design by <a target="_blank" href="http://www.wowthemes.net/premium-themes-templates/">WowThemesNet</a> </span>
                    </div>
                </div>
            </div>
        )
    }
}