import React, { Component } from "react";

export default class Navbar extends Component {
    render() {
        return(
            <div className={"row"}>
                <div className={"headerlogo four columns"}>
                    <div className={"logo"}>
                        <a href="/">
                        <h4>Nexsoft design task</h4>
                        </a>
                    </div>
                </div>
                <div className={"headermenu eight columns noleftmarg"}>
                    <nav id={"nav-wrap"}>
                        <ul id="main-menu" className={"nav-bar sf-menu"}>
                            <li className={"current"}>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/">Portofolio</a>
                            </li>
                            <li>
                                <a href="/">Blog</a>
                            </li>
                            <li>
                                <a href="/">Pages</a>
                            </li>
                            <li>
                                <a href="/">Features</a>
                            </li>
                            <li>
                                <a href="/">Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        )
    }
}