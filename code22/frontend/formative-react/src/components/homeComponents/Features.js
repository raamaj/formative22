import React, { Component } from "react";

export default class Features extends Component {
    render() {
        return(
            <div className="row">
                <div className="twelve columns">
                    <ul className="ca-menu">
                        <li>
                        <a href="#">
                        <span className="ca-icon"><i className="fa fa-heart"></i></span>
                        <div className="ca-content">
                            <h2 className="ca-main">Responsive<br/> Design</h2>
                            <h3 className="ca-sub">Across all major devices</h3>
                        </div>
                        </a>
                        </li>
                        <li>
                        <a href="#">
                        <span className="ca-icon"><i className="fa fa-bullhorn"></i></span>
                        <div className="ca-content">
                            <h2 className="ca-main">Friendly<br/> Documentation</h2>
                            <h3 className="ca-sub">Straight to the point</h3>
                        </div>
                        </a>
                        </li>
                        <li>
                        <a href="#">
                        <span className="ca-icon"><i className="fa fa-user"></i></span>
                        <div className="ca-content">
                            <h2 className="ca-main">Alternate<br/> Home Pages</h2>
                            <h3 className="ca-sub">Full slider, boxed or none</h3>
                        </div>
                        </a>
                        </li>
                        <li>
                        <a href="#">
                        <span className="ca-icon"><i className="fa fa-camera"></i></span>
                        <div className="ca-content">
                            <h2 className="ca-main">Filterable<br/> Portofolio</h2>
                            <h3 className="ca-sub">Isotop & PrettyPhoto</h3>
                        </div>
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}