import React, { Component } from "react";

export default class WhatWeDo extends Component {
    render() {
        return(
            <div className="row">
                <div className="twelve columns">
                    <div className="centersectiontitle">
                        <h4>What we do</h4>
                    </div>
                </div>
                <div className="four columns">
                    <h5>Photography</h5>
                    <p>
                        Swine short ribs meatball irure bacon nulla pork belly cupidatat meatloaf cow. Nulla corned beef sunt ball tip, qui bresaola enim jowl. Capicola short ribs minim salami nulla nostrud pastrami.
                    </p>
                    <p>
                        <a href="#" className="readmore">Learn more</a>
                    </p>
                </div>
                <div className="four columns">
                    <h5>Artwork</h5>
                    <p>
                        Swine short ribs meatball irure bacon nulla pork belly cupidatat meatloaf cow. Nulla corned beef sunt ball tip, qui bresaola enim jowl. Capicola short ribs minim salami nulla nostrud pastrami.
                    </p>
                    <p>
                        <a href="#" className="readmore">Learn more</a>
                    </p>
                </div>
                <div className="four columns">
                    <h5>Logos</h5>
                    <p>
                        Swine short ribs meatball irure bacon nulla pork belly cupidatat meatloaf cow. Nulla corned beef sunt ball tip, qui bresaola enim jowl. Capicola short ribs minim salami nulla nostrud pastrami.
                    </p>
                    <p>
                        <a href="#" className="readmore">Learn more</a>
                    </p>
                </div>
            </div>
        )
    }
}