// import './App.css';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import HomeComponents from './components/homeComponents/HomeComponents';

function App() {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<HomeComponents/>}/>
      </Routes>
    </Router>
  );
}

export default App;
