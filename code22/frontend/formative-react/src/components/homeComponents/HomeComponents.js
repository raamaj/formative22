import React, { Component } from "react";
import Navbar from "../commonComponents/Navbar";
import TopSlider from "../commonComponents/TopSlider";
import ImageSlider from "./ImageSlider";
import SubHeader from "./SubHeader";
import Features from "./Features";
import WhatWeDo from "./WhatWeDo";
import Quotes from "../commonComponents/Quotes";
import Footer from "../commonComponents/Footer";
import Copyright from "../commonComponents/Copyright";

export default class HomeComponents extends Component {
    render() {
        return(
            <>
                <TopSlider />
                <Navbar />
                <div className={"clear"}></div>
                <ImageSlider />
                <div className={"minipause"}></div>
                <SubHeader />
                <div className={"minipause"}></div>
                <Features />
                <div className={"minipause"}></div>
                <WhatWeDo />
                <div className="hr"></div>
                <Quotes />
                <Footer />
                <Copyright />
            </>
        )
    }
}